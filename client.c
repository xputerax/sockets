#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

char* sock_path = "mysocket.sock";

int main(int argc, char* argv[]) {
    /** initialize socket address **/
    struct sockaddr_un sock;
    memset(&sock, 0, sizeof(struct sockaddr_un));
    sock.sun_family = AF_UNIX;
    strncpy(sock.sun_path, sock_path, sizeof(sock.sun_path)-1);

    int sfd;
    sfd = socket(AF_UNIX, SOCK_STREAM, 0);

    if (sfd == -1) {
        printf("socket() failed: %s (%d)\n", strerror(errno), errno);
        exit(EXIT_FAILURE);
    }

    printf("socket opened (fd = %d)\n", sfd);

    int ret;
    struct sockaddr addr;
    ret = connect(sfd, (struct sockaddr *) &sock, sizeof(sock));

    if (ret == -1) {
        printf("connect() error: %s (%d)\n", strerror(errno), errno);
	exit(EXIT_FAILURE);
    } 

    printf("connect() success (code = %d)\n", ret);

    size_t sz;
    char* msg = "hello there";
    size_t msg_sz = sizeof(msg);

    sz = write(sfd, msg, msg_sz); 

    if (sz == -1) {
    	printf("error when writing (-1): %s (%d)\n", strerror(errno), errno);
	exit(EXIT_FAILURE);
    }

    printf("expecting %d bytes, sent %d bytes to fd = %d\n", msg_sz, sz, sfd);

    return 0;
}
