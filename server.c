#include <stdlib.h>
#include <stdio.h>
#include <string.h>
// #include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_CONN 1024
#define BUFFER_SIZE 1028

int main(int argc, char* argv[]) {
    struct sockaddr_un sockaddr;
    memset(&sockaddr, 0, sizeof(struct sockaddr_un));
    sockaddr.sun_family = AF_UNIX;
    char* socket_path = "mysocket.sock";
    strncpy(sockaddr.sun_path, socket_path, sizeof(sockaddr.sun_path) - 1);

    printf("Starting socket server...\n");

    struct stat buf;

    if (stat(sockaddr.sun_path, &buf) == 0) {
        printf("Removing old socket\n");

        if (unlink(sockaddr.sun_path) == 0) {
            printf("%s removed\n", sockaddr.sun_path);
        } else {
            printf("unlink error: %s (%d)\n", strerror(errno), errno);
            exit(errno);
        }
    }

    int sfd = socket(PF_LOCAL, SOCK_STREAM, 0);

    if (sfd == -1) {
        printf("Socket error: %s (%d)\n", strerror(errno), errno);
        exit(errno);
    }

    printf("socket fd = %d\n", sfd);


    if (bind(sfd, (struct sockaddr *) &sockaddr, sizeof(struct sockaddr_un)) == -1) {
        printf("bind error: %s (%d)\n", strerror(errno), errno);
        exit(errno);
    }

    int listen_err = listen(sfd, MAX_CONN);

    if (listen_err == -1) {
        printf("listen() error: %s (%d)\n", strerror(errno), errno);
        exit(listen_err);
    }

    printf("listening ...");

    ssize_t read_size;
    char read_buf[BUFFER_SIZE];

    for (;;) {
        printf("waiting for connection ...\n");

        int cfd = accept(sfd, NULL, NULL);

        printf("accepted connection (fd = %d)\n", cfd);

        while ((read_size = read(cfd, read_buf, BUFFER_SIZE)) > 0) {
            printf("read %d bytes: %s\n", read_size, read_buf);
            if (write(STDOUT_FILENO, read_buf, read_size) != read_size) {
                printf("error: partial/failed write");
            }
        }

        if (read_size == -1) {
            printf("read error: %s (%d)\n", strerror(errno), errno);
            exit(errno);
        }

        if (close(cfd) == -1) {
           printf("close error: %s (%d)\n", strerror(errno), errno);
        } else {
            printf("connection fd = %d closed\n", cfd);
        }
    }

}
